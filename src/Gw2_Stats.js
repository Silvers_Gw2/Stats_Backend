const { name, version, port, env, db } = require('./config.js')
const restify = require('restify')
const corsMiddleware = require('restify-cors-middleware')
const mongodb = require('mongodb').MongoClient

const server = restify.createServer({ name: name, version: version })

server.use(restify.plugins.bodyParser({ mapParams: true }))
server.use(restify.plugins.bodyParser({ mapParams: true }))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())

const cors = corsMiddleware({ origins: ['*'], allowHeaders: ['*'], exposeHeaders: ['*'] })
server.pre(cors.preflight)
server.use(cors.actual)

server.listen(port, () => {
  // establish connection to mongodb atlas
  mongodb.connect(db.uri, (err, client) => {

    if (err) {
      console.error('An error occurred while attempting to connect to MongoDB',err)
      process.exit(1)
    }
    const database = client.db(db.database)
    // Creates teh indexes to speed up all db operations
    database.collection('items').createIndex({ 'id': 1 })

    console.info(`${name} v${version} ready to accept connections on port ${port} in ${env} environment.`)

    require('./routeController.js')({ database, server })

  })
})