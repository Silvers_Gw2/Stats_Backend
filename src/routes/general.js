const { beautifyJSON, querySorter } = require('../functions')
const { key } = require('../config')

module.exports = function (ctx) {
  const db = ctx.database
  const server = ctx.server

  server.get('/gw2_stats/v1/general', async (req, res, next) => {

    const activates = await db.collection('general')
    .distinct('activity')
    .catch(err => res.send(500, err))

    res.sendRaw(200, beautifyJSON(activates, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })

    next()
  })

  server.get('/gw2_stats/v1/general/:activity', async (req, res, next) => {
    let activity = req.params.activity || ''

    let find;
    if (req.header('auth') !== key && req.query.auth !== key) {
      find = { activity: activity, dateAvailable: { $lte: Date.now() } }
    } else {
      find = { activity: activity, dateAvailable: { $gte: Date.now() } }
    }
    const ids = await db.collection('general')
    .distinct('consumedID', find)
    .catch(err => res.send(500, err))

    res.sendRaw(200, beautifyJSON(ids, 'human'), { 'Content-Type': 'application/json; charset=utf-8' })

    next()
  })

  server.get('/gw2_stats/v1/general/:activity/:id', async (req, res, next) => {

    let id = req.params.id
    let activity = req.params.activity || ''
    let beautify = req.query.beautify || 'human'

    let find = { consumedID: 0 }
    if (id !== 'bulk') {
      id = id - 0
      if (req.header('auth') !== key && req.query.auth !== key) {
        find = {
          consumedID: id,
          activity: activity,
          dateAvailable: { $lte: Date.now() },
        }
      } else {
        find = {
          consumedID: id,
          activity: activity,
          dateAvailable: { $gte: Date.now() },
        }
      }
    } else {
      let bulk = querySorter(req.query, {
        beautify: true,
        fields: true,
        project: true,
        filter: true,
        ids: true,
        start: false,
        end: false,
      }, 'general')
      if (typeof bulk.find.$and !== 'undefined' && typeof bulk.find.$and[0] !==
        'undefined') {
        let editing = bulk.find.$and[0].$or
        for (let i = 0; i < editing.length; i++) {
          editing[i].consumedID = editing[i].id
          delete editing[i].id
        }
        bulk.find.$and.push(
          { activity: activity, dateAvailable: { $lte: Date.now() } })
        find = bulk.find
      }
    }
    db.collection('general')
    .find(find)
    .project({ _id: 0, consumedID: 0, id: 0, id0: 0, dateAvailable: 0, accountArchive: 0, })
    .toArray()
    .then(result => res.sendRaw(200, beautifyJSON(result, beautify), { 'Content-Type': 'application/json; charset=utf-8' }))
    .catch(err => res.send(500, err))

    next()
  })
}